walls.register("walls_all:brick", "Brick Wall", "default_brick.png",
		"default:brick", default.node_sound_stone_defaults())
		
walls.register("walls_all:desert_stone", "Desert Stone Wall", "default_desert_stone.png",
		"default:desert_stone", default.node_sound_stone_defaults())
		
walls.register("walls_all:desert_stonebrick", "Desert Stone Brick Wall", "default_desert_stone_brick.png",
		"default:desert_stonebrick", default.node_sound_stone_defaults())
		
walls.register("walls_all:desert_stone_block", "Desert Stone Block Wall", "default_desert_stone_block.png",
		"default:desert_stone_block", default.node_sound_stone_defaults())
		
walls.register("walls_all:desert_sandstone", "Desert Sandstone Wall", "default_desert_sandstone.png",
		"default:desert_sandstone", default.node_sound_stone_defaults())
		
walls.register("walls_all:desert_sandstone_brick", "Desert Sandstone Brick Wall", "default_desert_sandstone_brick.png",
		"default:desert_sandstone_brick", default.node_sound_stone_defaults())
		
walls.register("walls_all:desert_sandstone_block", "Desert Sandstone Block Wall", "default_desert_sandstone_block.png",
		"default:desert_sandstone_block", default.node_sound_stone_defaults())
		
walls.register("walls_all:silver_sandstone", "Silver Sandstone Wall", "default_silver_sandstone.png",
		"default:silver_sandstone", default.node_sound_stone_defaults())
		
walls.register("walls_all:silver_sandstone_brick", "Silver Sandstone Brick Wall", "default_silver_sandstone_brick.png",
		"default:silver_sandstone_brick", default.node_sound_stone_defaults())
		
walls.register("walls_all:silver_sandstone_block", "Silver Sandstone Block Wall", "default_silver_sandstone_block.png",
		"default:silver_sandstone_block", default.node_sound_stone_defaults())
		
walls.register("walls_all:sandstone", "Sandstone Wall", "default_sandstone.png",
		"default:sandstone", default.node_sound_stone_defaults())
		
walls.register("walls_all:sandstonebrick", "Sandstone Brick Wall", "default_sandstone_brick.png",
		"default:sandstonebrick", default.node_sound_stone_defaults())
		
walls.register("walls_all:sandstone_block", "Sandstone Block Wall", "default_sandstone_block.png",
		"default:sandstone_block", default.node_sound_stone_defaults())
		
walls.register("walls_all:obsidian", "Obsidian Wall", "default_obsidian.png",
		"default:obsidian", default.node_sound_stone_defaults())
		
walls.register("walls_all:obsidianbrick", "Obsidian Brick Wall", "default_obsidian_brick.png",
		"default:obsidianbrick", default.node_sound_stone_defaults())
		
walls.register("walls_all:obsidian_block", "Obsidian Block Wall", "default_obsidian_block.png",
		"default:obsidian_block", default.node_sound_stone_defaults())
		
walls.register("walls_all:stone", "Stone Wall", "default_stone.png",
		"default:stone", default.node_sound_stone_defaults())
		
walls.register("walls_all:stonebrick", "Stone Brick Wall", "default_stone_brick.png",
		"default:stonebrick", default.node_sound_stone_defaults())
		
walls.register("walls_all:stone_block", "Stone Block Wall", "default_stone_block.png",
		"default:stone_block", default.node_sound_stone_defaults())
		
if minetest.get_modpath("ethereal") then
    walls.register("walls_all:icebrick", "Ice Brick Wall", "ethereal_brick_ice.png",
        "ethereal:icebrick", default.node_sound_glass_defaults())
        
	walls.register("walls_all:snowbrick", "Snow Brick Wall", "ethereal_brick_snow.png",
        "ethereal:snowbrick", default.node_sound_dirt_defaults())
end

if minetest.get_modpath("amethyst_new") then
    walls.register("walls_all:basalt", "Basalt Wall", "amethyst_basalt.png",
        "amethyst_new:basalt", default.node_sound_stone_defaults())
        
	walls.register("walls_all:basalt_block", "Basalt Block Wall", "amethyst_basalt_block.png",
        "amethyst_new:basalt_block", default.node_sound_stone_defaults())
        
	walls.register("walls_all:basalt_brick", "Basalt Brick Wall", "amethyst_basalt_brick.png",
        "amethyst_new:basalt_brick", default.node_sound_stone_defaults())
        
	walls.register("walls_all:calcite", "Calcite Wall", "amethyst_calcite.png",
        "amethyst_new:calcite", default.node_sound_stone_defaults())
        
	walls.register("walls_all:calcite_block", "Calcite Block Wall", "amethyst_calcite_block.png",
        "amethyst_new:calcite_block", default.node_sound_stone_defaults())
        
	walls.register("walls_all:calcite_brick", "Calcite Brick Wall", "amethyst_calcite_brick.png",
        "amethyst_new:calcite_brick", default.node_sound_stone_defaults())
end

--Compatibility with defunct walls_all mod
minetest.register_alias("walls_all:desert_stone_brick", "walls_all:desert_stonebrick")
minetest.register_alias("walls_all:sandstonestone_brick", "walls_all:sandstonebrick")
minetest.register_alias("walls_all:obsidian_brick", "walls_all:obsidianbrick")
minetest.register_alias("walls_all:stonestone_brick", "walls_all:stonebrick")
